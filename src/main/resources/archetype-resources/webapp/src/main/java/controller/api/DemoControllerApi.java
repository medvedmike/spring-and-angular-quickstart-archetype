#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ${package}.dto.DemoDTO;
import ${package}.dto.DemoView;
import ${package}.service.DemoService;
import ${package}.util.GridBean;

@Controller
public class DemoControllerApi extends BaseControllerApi {

	@Autowired
	private DemoService demoService;

	@RequestMapping(value = "/demos", method = RequestMethod.GET)
	@ResponseBody
	public GridBean getDemos(@RequestParam("page") Long page, @RequestParam("rows") Long rows) {
		return demoService.getDemos(page, rows);
	}

	@RequestMapping(value = "/demos/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<DemoDTO> getDemo(@PathVariable("id") Long id) {
		DemoDTO dto = demoService.getDemo(id);
		if (dto != null)
			return new ResponseEntity<>(dto, HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/demos", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<DemoDTO> postDemo(@RequestBody DemoDTO dto) {
		return new ResponseEntity<>(demoService.createDemo(dto),
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/demos/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<?> putDemo(@PathVariable("id") Long id,
			@RequestBody DemoDTO dto) {
		if (!id.equals(dto.getId()))
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		demoService.updateDemo(dto);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/demos/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<?> deleteDemo(@PathVariable("id") Long id) {
		demoService.deleteDemo(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
