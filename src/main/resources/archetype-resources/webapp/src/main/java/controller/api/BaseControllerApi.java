#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.controller.api;

import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/api")
public class BaseControllerApi {

}
