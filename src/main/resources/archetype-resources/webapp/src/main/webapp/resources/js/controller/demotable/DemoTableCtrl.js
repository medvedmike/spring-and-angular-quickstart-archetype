(function() {
	'use strict';
	angular.module('webmodule').controller('DemoTableCtrl',
			[ '$scope', 'DemoService', '$state', 'GridService', 'ModalService', 
			  function($scope, DemoService, $state, GridService, ModalService) {
				
				$scope.gridHeaders = [ {
					id: "id",
		            label: "№",
		            visible: true,
		            disableSort: true
				}, {
					id: "name",
		            label: "Name",
		            visible: true
				}, {
					id: "flag",
		            label: "Flag",
		            visible: true
				}, {
					id: "duration",
		            label: "Duration",
		            visible: true
				}, {
					id: "cost",
		            label: "Cost",
		            visible: true
				} ];

				var GRID_NAME = $state.current.name + 'demosgrid';
				
		        /* save params begin */

		        var gridParams = GridService.getGridParamsById(GRID_NAME);
		        var start = 0;
		        var number = 10;
		        var predicate = "name";
		        var reverse = true;

		        if (gridParams != null) {
		            start = angular.isDefined(gridParams.start) ? gridParams.start : start;
		            number = angular.isDefined(gridParams.number) ? gridParams.number : number;
		            predicate = angular.isDefined(gridParams.predicate) ? gridParams.predicate : predicate;
		            reverse = angular.isDefined(gridParams.reverse) ? gridParams.reverse : reverse;

		            if (angular.isDefined(gridParams.filters)) {
		                
		            }
		        } else {
		            saveGridParams();
		        }

		        function saveGridParams() {
		            GridService.setGridParams(GRID_NAME,{
		                start: start,
		                number: number,
		                predicate: predicate,
		                reverse: reverse,
		                filters: {
		                    
		                }
		            });
		        }

		        /* save params end */

		        /* Pagination begin */

		        var currentColOfAddedPages = 0;
		        /**
		         * Подгружает следующую порцию данных в грид на той же странице.
		         *
		         * @param stItemsByPage - по сколько данных подгружать
		         */
		        $scope.setColOnPage = function (stItemsByPage) {
		            $scope.viewRowsOnPage += stItemsByPage;
		            setAddPageParams();
		            
		            var params = {
		            		pagination: {
		            			start: start,
		            			number: number
		            		},
		            		sort: {
		            			predicate: predicate,
		            			reverse: reverse
		            		},
		            		query: getParams()
		            };
		            
		            DemoService.getDemos(params).then(function (result) {
		                saveGridParams();
		                angular.forEach(result.rows, function (value) {
		                    $scope.demos.push(value);
		                });
		            });
		        };

		        var currentPage = 1;
		        var colOfRecords = 0;

		        /**
		         * Посылает пагинатору актуальную информацию о гриде.
		         */
		        function sendGridInfoToPaginator() {
		            $scope.$broadcast('gridInfoForPagination', {
		                currentColOfAddedPages: currentColOfAddedPages,
		                colOfRecords: colOfRecords,
		                setColOnPage: $scope.setColOnPage,
		                setCurrentPage: function(page) {
		                    currentPage = page;
		                }
		            });
		        }

		        /**
		         * Устанавливает параметры пагинации.
		         */
		        function setAddPageParams() {
		            currentColOfAddedPages++;
		            sendGridInfoToPaginator();
		            if ($scope.tableState && $scope.tableState != null) {
		                var pagination = $scope.tableState.pagination;
		                start = pagination.start + pagination.number*currentColOfAddedPages;
		                number = pagination.number;
		                predicate = $scope.tableState.sort.predicate;
		                reverse = $scope.tableState.sort.reverse;
		            }
		        }

		        $scope.refreshDemos = function () {
		            if (angular.isDefined($scope.viewRowsOnPage)) {
		                if ($scope.colRowsOnPage != $scope.viewRowsOnPage) {
		                    $scope.colRowsOnPage = $scope.viewRowsOnPage;
		                } else {
		                    $scope.getDemos();
		                }
		            }
		        };

		        /* Pagination end */

		        $scope.isShowColumn = function (index) {
		            return $scope.gridHeaders[index].visible;
		        };

		        $scope.clearFilters = function() {
		            clearFields();
		            $scope.getDemos();
		            saveGridParams();
		        };

		        function clearFields() {
		            
		        }

		        var isFindInProcess = false;
		        $scope.findAgencies = function() {
		            isFindInProcess = true;
		            GridService.resetPageParams($scope.tableState);
		            $scope.getAgencies($scope.tableState, 10, true);
		        };

		        $scope.isShowClearButton = function() {
		            return false;
		        };

		        $scope.$watch(function () {
		            return $scope.colRowsOnPage;
		        }, function (newColPages, oldColPages) {
		            if (newColPages != null && newColPages != oldColPages) {
		                $scope.getDemos(null, parseInt(newColPages))
		            }
		        });

		        $scope.rowCollection = [];
		        $scope.demos = [].concat($scope.rowCollection);
		        $scope.tableState = null;

		        $scope.getDemos = function(tableState, colPages, isFirstPage) {
		            currentColOfAddedPages = 0;
		            if ($scope.tableState == null) {
		                $scope.tableState = tableState ? tableState : {};
		                if (!$scope.tableState.pagination)
		                	$scope.tableState.pagination = {};
		                $scope.tableState.pagination.start = start;
		            }
		            $scope.isLoading = true;
		            if ($scope.tableState && $scope.tableState != null) {
		                GridService.setGridTableStateParams($scope.tableState, predicate, reverse); // установка параметров пагинации по умолчанию
		                var pagination = $scope.tableState.pagination;
		                if (!isFirstPage) {
		                    start = typeof  pagination.start == 'number' ? pagination.start : start;
		                } else {
		                    $scope.tableState.pagination.start = 0;
		                }
		                number = typeof  pagination.number == 'number' ? pagination.number : number;
		                predicate = $scope.tableState.sort.predicate;
		                reverse = $scope.tableState.sort.reverse;
		            }
		            number = typeof colPages == "number" ? colPages : number;

		            var params = {
		            		pagination: {
		            			start: start,
		            			number: number
		            		},
		            		sort: {
		            			predicate: predicate,
		            			reverse: reverse
		            		},
		            		query: getParams()
		            };
		            
		            DemoService.getDemos(params).then(function (result) {
		                saveGridParams();
		                $scope.demos = result.rows;
		                $scope.tableState.pagination.numberOfPages = result.total;
		                $scope.viewRowsOnPage = $scope.colRowsOnPage;
		                $scope.isLoading = false;
		                colOfRecords = result.records;
		                sendGridInfoToPaginator();
		            });
		        };

		        function getParams() {
		            return {};
		        }
				
				$scope.createDemo = function() {
					ModalService.open({
						template: 'demotable/editDemoDlg.html',
						controller: 'EditDemoCtrl',
						size: 'md',
						data: {
							title: "Create Demo"
						}
					}).result.then(function() {
						$scope.getDemos();
					});
				};
				
				$scope.editDemo = function(id) {
					ModalService.open({
						template: 'demotable/editDemoDlg.html',
						controller: 'EditDemoCtrl',
						size: 'md',
						data: {
							title: "Edit Demo",
							id: id
						}
					}).result.then(function() {
						$scope.getDemos();
					});
				};
				
				$scope.removeDemo = function(id) {
					DemoService.deleteDemo(id).then(function() {
						$scope.getDemos();
					});
				};
				
				$scope.getIndex = function(index) {
		            if (!$scope.tableState || $scope.tableState == null)
		                return index;
		            return $scope.tableState.pagination.start + index + 1;
		        }
				
			} ]);
})();