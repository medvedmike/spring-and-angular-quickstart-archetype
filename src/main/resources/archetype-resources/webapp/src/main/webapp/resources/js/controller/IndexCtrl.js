(function() {
	'use strict';
	angular.module('webmodule').controller('IndexCtrl',
			[ '$scope', 'ModalService', function($scope, ModalService) {
				
				$scope.showModal = function() {
					var modal = ModalService.open({
						size: 'md',
						controller: 'StubModalCtrl',
						template: 'demo/stubModal.html',
						data: {
							title: "Stub modal!"
						}
					});
				}
				
			} ]);
})();