#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html ng-app="webmodule">
<head>
	<base href='<c:url value="/web"/>'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
	<link rel="stylesheet" href="<c:url value="/resources/css/main.css"/>">
	<link rel="stylesheet" href="<c:url value="/resources/lib/bootstrap/css/bootstrap.min.css"/>">
	
	<title>${applicationName}</title>
</head>
<body ng-controller="MainCtrl">
	
	<div id="header" ng-controller="HeaderCtrl" ng-include="'<c:url value="/resources/templates/header.html"/>'"></div>
	<div class="container-fluid main-container" ui-view></div>
	<div ng-include="'<c:url value="/resources/templates/footer.html"/>'"></div>

	<script type="text/javascript" src='<c:url value="/resources/lib/jquery/jquery-2.1.4.min.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/lib/jquery/jquery-ui.min.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/lib/bootstrap/js/bootstrap.min.js"/>'></script>

	<script type="text/javascript" src='<c:url value="/resources/lib/angular/angular.min.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/lib/angular/angular-route.min.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/lib/angular/angular-ui-router.min.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/lib/angular/angular-breadcrumb.min.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/lib/angular/lodash.min.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/lib/angular/restangular.min.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/lib/angular/smart-table.min.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/lib/bootstrap/ui-bootstrap-tpls-0.14.3.min.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/lib/angular/angular-local-storage.min.js"/>'></script>
	
	<script type="text/javascript" src='<c:url value="/resources/js/app.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/run.js"/>'></script>
	
	<script type="text/javascript" src='<c:url value="/resources/js/directive/DraggableModal.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/directive/CenterModal.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/directive/grid/GridMenu.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/directive/grid/GridPagination.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/directive/grid/Grid.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/directive/SmartSort.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/directive/Loader.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/directive/Tooltip.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/directive/ConfirmClick.js"/>'></script>
	
	<script type="text/javascript" src='<c:url value="/resources/js/service/util/ModalService.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/service/util/GridService.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/service/DemoService.js"/>'></script>
	
	<script type="text/javascript" src='<c:url value="/resources/js/controller/MainCtrl.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/controller/HeaderCtrl.js"/>'></script>
	
	<script type="text/javascript" src='<c:url value="/resources/js/controller/IndexCtrl.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/controller/StubModalCtrl.js"/>'></script>
	
	<script type="text/javascript" src='<c:url value="/resources/js/controller/demotable/DemoTableCtrl.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/controller/demotable/EditDemoCtrl.js"/>'></script>
	
	<script type="text/javascript" src='<c:url value="/resources/js/controller/util/ConfirmDialogCtrl.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/controller/util/GridMenuCtrl.js"/>'></script>

</body>
</html>