(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name webmodule.controller:ConfirmDialogCtrl
	 * @description
	 *
	 * Контроллер диалога подтверждения.
	 */
	angular.module('webmodule')
	    .controller('ConfirmDialogCtrl', ['$scope', '$uibModalInstance', 'data',
	        function ($scope, $modalInstance, data) {
	            $scope.title = data.title;
	            $scope.message = data.message;
	            $scope.aTitle = data.aTitle;
	            $scope.aHref = data.aHref;

	            $scope.close = function () {
	                $modalInstance.dismiss('cancel');
	            };

	            $scope.save = function () {
	                $modalInstance.close({
	                });
	            };

	        }]);
})();