(function() {
    'use strict';

    /**
     * @ngdoc directive
     * @name armada.directive:DateRange
     * @description
     * Директива для обработки диапазона дат
     */
    angular.module('webmodule').
        directive('wmGridMenu', ['$timeout', 'templatePrefix', function ($timeout, templatePrefix) {
            return {
                restrict: 'A',
                replace: true,
                scope: {
                    refreshAction: '=',
                    rowsOnPage: '=',
                    headers: '=',
                    viewRowsOnPage: '='
                },
                controller: 'GridMenuCtrl',
                templateUrl: templatePrefix + '/directives/grid/gridMenu.html'
            }
        }]);
})();