(function() {
	'use strict';
	angular.module('webmodule').service('ModalService', 
			[
			 '$uibModal',
			 'templatePrefix',
			 function($uibModal, templatePrefix) {
				 return {
					 open: function(conf) {
						 return $uibModal.open({
							 templateUrl: templatePrefix + '/dialogs/' + conf.template,
							 controller: conf.controller,
							 windowTemplateUrl: templatePrefix + '/dialogs/util/draggableTemplate.html',
							 size: conf.size,
							 resolve: {
								 data: typeof conf.data === 'function' ? 
										 conf.data : 
										 function () {
											 return conf.data
										 }
							 }
						 });
					 }
				 };
			 }]);
})();