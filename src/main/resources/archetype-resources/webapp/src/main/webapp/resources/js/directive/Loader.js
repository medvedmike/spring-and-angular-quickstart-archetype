angular.module('webmodule').
	    directive('wmLoader', ['templatePrefix', function (templatePrefix) {
	        return {
	            restrict: 'E',
	            template: '<img ng-src="{{::getSrc()}}">',
	            link: function(scope, element, attrs) {
	                scope.getSrc = function() {
	                	return templatePrefix + '/../img/ajax-loader-medium.gif'
	                }
	            }
	        }
	    }]);