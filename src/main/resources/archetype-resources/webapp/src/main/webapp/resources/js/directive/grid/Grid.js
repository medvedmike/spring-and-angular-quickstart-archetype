(function() {
    'use strict';

    angular.module('webmodule').controller('GridCtrl',
			[ '$scope',
			  function($scope) {
				
				
				
			} ]);
    
    /**
     * @ngdoc directive
     * @name armada.directive:DateRange
     * @description
     * Директива для обработки диапазона дат
     */
    angular.module('webmodule').
        directive('wmGrid', ['$timeout', 'templatePrefix', function ($timeout, templatePrefix) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    id: '='
                },
                controller: 'GridCtrl',
                templateUrl: templatePrefix + '/directives/grid/grid.html'
            }
        }]);
})();