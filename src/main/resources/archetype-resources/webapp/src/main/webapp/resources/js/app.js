(function() {
	'use strict';
	var module = angular.module('webmodule', [ 'ngRoute', 
	                               'ui.router', 
	                               'ncy-angular-breadcrumb', 
	                               'restangular',
	                               'ui.bootstrap',
	                               'smart-table',
	                               'LocalStorageModule']).config(
			function($stateProvider, $urlRouterProvider, $locationProvider, RestangularProvider, localStorageServiceProvider) {

				$locationProvider.html5Mode(true).hashPrefix('!');
				
				localStorageServiceProvider.setPrefix('webmodule');
				
				var domain = "";
				
				RestangularProvider.setBaseUrl(domain + 'api');

				var defaultHeaders = {
						'X-Requested-With': 'XMLHttpRequest' // this is ajax request
				};

				if (isIe11()) {
					defaultHeaders["If-Modified-Since"] = "Mon, 26 Jul 1997 05:00:00 GMT";
				}
				RestangularProvider.setDefaultHeaders(defaultHeaders);

				function isIe11() {
					var trident = !!navigator.userAgent.match(/Trident\/7.0/);
					var rv = navigator.userAgent.indexOf("rv:11.0");
					return !!(trident && rv != -1);
				}

				$urlRouterProvider.otherwise(domain + '/web');

				var prefix = "/web";
				var templatePrefix = domain + "resources/templates";

				$stateProvider.state('index', {
					url : prefix,
					templateUrl : templatePrefix + "/views/index.html",
					controller : "IndexCtrl",
					ncyBreadcrumb : {
						label : 'Index page'
					}
				})
				.state('demotable', {
					url : prefix + '/demotable',
					templateUrl : templatePrefix + "/views/demotable.html",
					controller : "DemoTableCtrl",
					ncyBreadcrumb : {
						label : 'Demo table page',
						parent: 'index'
					}
				});

			});
	module.value('templatePrefix', 'resources/templates');

})();