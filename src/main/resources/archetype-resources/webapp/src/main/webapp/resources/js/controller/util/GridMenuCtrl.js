(function() {
	(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name webmodule.controller:ConfirmDialogCtrl
	 * @description
	 *
	 * Контроллер диалога подтверждения.
	 */
	angular.module('webmodule')
	    .controller('GridMenuCtrl', ['$scope', '$timeout', function ($scope, $timeout) {
            $scope.viewRowsOnPage = $scope.rowsOnPage;  // init
            $scope.pageRowsNum = [10, 50, 100, 250, 500, 1000];
            angular.element(".show-hide-grid-cols").tooltip({
                title : 'Скрыть/показать столбцы',
                placement: 'top',
                container: 'body'
            });
            angular.element(".grid-rows-on-page").tooltip({
                title : 'Сменить количество строк',
                placement: 'top',
                container: 'body'
            });
            $scope.selectPageRowsNum = function(pageRowsNum) {
                if ($scope.rowsOnPage == pageRowsNum && $scope.rowsOnPage != $scope.viewRowsOnPage) {
                    $scope.viewRowsOnPage = $scope.rowsOnPage;
                    $timeout(function(){
                        $scope.refreshAction();
                    });
                }
                $scope.rowsOnPage = pageRowsNum;
                $scope.viewRowsOnPage = pageRowsNum;
                return false;
            }
        }]);
})();
})();