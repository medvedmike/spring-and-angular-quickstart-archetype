#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service;

import java.util.List;

import ${package}.dto.DemoDTO;
import ${package}.dto.DemoView;
import ${package}.util.GridBean;

public interface DemoService {

	public GridBean getDemos(long page, long count);

	public DemoDTO getDemo(Long id);

	public DemoDTO createDemo(DemoDTO dto);

	public void updateDemo(DemoDTO dto);

	public void deleteDemo(Long id);

}