#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.util;

import org.codehaus.jackson.annotate.JsonProperty;

public class GridBean {

	@JsonProperty("total")
	private long   totalPagesNumber = 0;

	@JsonProperty("page")
	private long   currentPage      = 0;

	@JsonProperty("records")
	private long   totalDBRecords   = 0;

	@JsonProperty("rows")
	private Object object           = null;

	public GridBean () {
	}

	public GridBean (Object rows, long totalDBRecords, long rowsOnPage, long currentPage) {
		double pageCeil =  Math.ceil((double)totalDBRecords / rowsOnPage);

		this.totalPagesNumber = (pageCeil == 0) ? 1 : Math.round(pageCeil);
		this.currentPage      = currentPage;
		this.totalDBRecords   = totalDBRecords;
		this.object           = rows;
	}
	
	public GridBean (long totalDBRecords, long rowsOnPage, long currentPage) {
		double pageCeil =  Math.ceil((double)totalDBRecords / rowsOnPage);

		this.totalPagesNumber = (pageCeil == 0) ? 1 : Math.round(pageCeil);
		this.currentPage      = currentPage;
		this.totalDBRecords   = totalDBRecords;
	}

	public long getTotalPagesNumber() {
		return totalPagesNumber;
	}

	public void setTotalPagesNumber(long totalPagesNumber) {
		this.totalPagesNumber = totalPagesNumber;
	}

	public long getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(long currentPage) {
		this.currentPage = currentPage;
	}

	public long getTotalDBRecords() {
		return totalDBRecords;
	}

	public void setTotalDBRecords(long totalDBRecords) {
		this.totalDBRecords = totalDBRecords;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object rows) {
		this.object = rows;
	}

}
