#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Collection;

import org.springframework.stereotype.Service;

import ${package}.dto.DemoDTO;
import ${package}.dto.DemoView;
import ${package}.service.DemoService;
import ${package}.service.BaseService;
import ${package}.util.GridBean;

@Service
public class DemoServiceImpl extends BaseService implements DemoService {
	
	private static Map<Long, DemoDTO> demos = new HashMap<>();
	private static Long nextId = null; 
	
	static {
		demos.put(1L, new DemoDTO(1L, "Name 1", true, 5, 2.5f));
		demos.put(2L, new DemoDTO(2L, "Name 2", false, 2133, 222.1125f));
		demos.put(3L, new DemoDTO(3L, "Name 3", false, 31, 121.5f));
		demos.put(4L, new DemoDTO(4L, "Name 4", true, 1243321, 0.2125f));
		demos.put(5L, new DemoDTO(5L, "Name 5", false, 53, 23.5f));
		demos.put(6L, new DemoDTO(6L, "Name 6", false, 2123, 282.115f));
		demos.put(7L, new DemoDTO(7L, "Name 7", true, 31, 121.5f));
		demos.put(8L, new DemoDTO(8L, "Name 8", false, 123281, 0.27125f));
		demos.put(9L, new DemoDTO(9L, "Name 9", true, 55, 2.95f));
		demos.put(10L, new DemoDTO(10L, "Name 10", true, 2183, 227.115f));
		demos.put(11L, new DemoDTO(11L, "Name 11", false, 381, 1241.5f));
		demos.put(12L, new DemoDTO(12L, "Name 12", true, 12321, 80.2125f));
		demos.put(13L, new DemoDTO(13L, "Name 13", true, 538, 2.55f));
		demos.put(14L, new DemoDTO(14L, "Name 14", true, 213, 22.115f));
		demos.put(15L, new DemoDTO(15L, "Name 15", false, 361, 121.5f));
		demos.put(16L, new DemoDTO(16L, "Name 16", true, 125321, 0.2125f));
		nextId = 17L;
	}
	
	@Override
	public GridBean getDemos(long page, long count) {
		List<DemoView> views = new ArrayList<>();
		Long num = 0L;
		Long skip = (page-1) * count;
		Long cnt = 0L;
		Collection<DemoDTO> collection = demos.values();
		for (DemoDTO dto : collection) {
			if (num < skip) {
				num++;
				continue;
			}
			DemoView view = new DemoView(dto.getId(), dto.getName(), dto.getFlag(), dto.getDuration(), dto.getCost());
			view.setRnum(num);
			view.setCnt((long)demos.size());
			views.add(view);
			num++;
			if (cnt++ >= count - 1)
				break;
		}
		return createGridBean(views, page, count);
	}
	
	@Override
	public DemoDTO getDemo(Long id) {
		DemoDTO original = demos.get(id);
		if (original == null)
			throw new NullPointerException();
		return original;
	}
	
	@Override
	public DemoDTO createDemo(DemoDTO dto) {
		dto.setId(nextId++);
		demos.put(dto.getId(), dto);
		return dto;
	}
	
	@Override
	public void updateDemo(DemoDTO dto) {
		DemoDTO original = demos.get(dto.getId());
		if (original == null)
			throw new NullPointerException();
		original.setCost(dto.getCost());
		original.setDuration(dto.getDuration());
		original.setFlag(dto.getFlag());
		original.setName(dto.getName());
	}
	
	@Override
	public void deleteDemo(Long id) {
		demos.remove(id);
	}
	
}
