#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.dto;

public class DemoView extends PageSupport {
	private Long id;
	private String name;
	private Boolean flag;
	private Integer duration;
	private Float cost;
	
	public DemoView() {
		
	}
	
	public DemoView(Long id, String name, Boolean flag, Integer duration,
			Float cost) {
		super();
		this.id = id;
		this.name = name;
		this.flag = flag;
		this.duration = duration;
		this.cost = cost;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getFlag() {
		return flag;
	}
	public void setFlag(Boolean flag) {
		this.flag = flag;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public Float getCost() {
		return cost;
	}
	public void setCost(Float cost) {
		this.cost = cost;
	}
}
