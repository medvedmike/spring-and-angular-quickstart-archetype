#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ${package}.util.GridBean;
import ${package}.dto.PageSupport;

/**
 * Базовый класс всех сервисов. Содержит общие методы и вызовы для всех сервисов.
 */
public abstract class BaseService {
	
	// /**
	// * Возвращает логгер.
	// * @return
	// */
	// protected abstract Logger getLogger();
	
	protected GridBean createGridBean(List<? extends PageSupport> views, Long page, Long count){
		if (views != null && views.size() > 0) {
			Long totalCount = views.get(0).getCnt();
			GridBean bean = new GridBean(views, totalCount, count, page);
			return bean;
		}
		return new GridBean(views, 0, count, page);
	}
}
